const { Schema } = require("mongoose");

const GroupSchema = new Schema(
  {
    active: {
      type: Boolean,
      default: true,
    },
    name: {
      type: String,
      required: true,
    },
    description: String,
    icon: String,
    photo: String,
    type: {
      type: String,
      required: true,
      match: /(public|private|secret)/gi,
    },
    members: {
      type: [Schema.Types.ObjectId],
      ref: "User",
      validate: (array) => array == null || array.length > 0,
    },
    admins: {
      type: [Schema.Types.ObjectId],
      ref: "User",
      validate: (array) => array == null || array.length > 0,
    },
    allowPublications: {
      type: Boolean,
      default: true,
    },
    allowEvents: {
      type: Boolean,
      default: true,
    },
  },
  {
    collection: "groups",
    minimize: false,
    versionKey: false,
  }
).set("toJSON", {
  transform: (doc, ret) => {
    ret.id = ret._id;
    delete ret._id;
  },
});

module.exports = GroupSchema;
