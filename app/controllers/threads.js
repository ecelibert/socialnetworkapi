const ThreadModel = require("../models/thread.js");

module.exports = class Threads {
  constructor(app, connect) {
    this.app = app;
    this.ThreadModel = connect.model("Thread", ThreadModel);
    this.run();
  }

  create() {
    this.app.post("/threads/", (req, res) => {
      try {
        const threadModel = new this.ThreadModel(req.body);

        threadModel
          .save()
          .then((thread) => {
            res.status(200).json(thread || {});
            console.log("Creating thread with _id " + thread._id);
          })
          .catch((err) => {
            res.status(400).json({
              code: 400,
              message: `ERROR post:threads -> ${err}`,
            });
            console.error(`ERROR post:threads -> ${err}`);
          });
      } catch (err) {
        res.status(400).json({
          code: 400,
          message: "Bad request post:threads",
        });
        console.error(`ERROR post:threads -> ${err}`);
      }
    });
  }

  readAll() {
    this.app.get("/threads/", (req, res) => {
      try {
        this.ThreadModel.find()
          .then((thread) => {
            res.status(200).json(thread);
            console.log("Returning all threads");
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR read:threads/ -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR read:threads/ -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  readId() {
    this.app.get("/threads/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the read:threads/:id query",
          });
          return;
        }

        this.ThreadModel.findById(req.params.id)
          .then((thread) => {
            res.status(200).json(thread || {});
            if (thread) console.log("Returning thread with _id " + thread._id);
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR read:threads/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR read:threads/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  update() {
    this.app.put("/threads/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the put:threads/:id query",
          });
          return;
        }

        const options = { new: true, runValidators: true };

        this.ThreadModel.findByIdAndUpdate(req.params.id, req.body, options)
          .then((threadUpdated) => {
            res.status(200).json(threadUpdated || {});
            if (threadUpdated)
              console.log(
                "Returning thread updated with _id " + threadUpdated._id
              );
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR put:threads/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR put:threads/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  delete() {
    this.app.delete("/threads/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the delete:threads/:id query",
          });
          return;
        }

        const options = { new: true, runValidators: true };

        this.ThreadModel.findByIdAndUpdate(
          req.params.id,
          { active: false },
          options
        )
          .then((thread) => {
            res.status(200).json(thread || {});
            if (thread) console.log("Deleting thread with _id " + thread._id);
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR delete:threads/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR delete:threads/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  run() {
    this.create();
    this.readAll();
    this.readId();
    this.update();
    this.delete();
  }
};
