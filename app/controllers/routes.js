const Users = require("./users.js");
const Events = require("./events.js");
const Groups = require("./groups.js");
const Threads = require("./threads.js");
const Albums = require("./albums.js");
const Polls = require("./polls.js");

module.exports = {
  Users,
  Events,
  Groups,
  Threads,
  Albums,
  Polls,
};
