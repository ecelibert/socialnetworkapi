const PollModel = require("../models/poll.js");

module.exports = class Polls {
  constructor(app, connect) {
    this.app = app;
    this.PollModel = connect.model("Poll", PollModel);
    this.run();
  }

  create() {
    this.app.post("/polls/", (req, res) => {
      try {
        const pollModel = new this.PollModel(req.body);

        pollModel
          .save()
          .then((poll) => {
            res.status(200).json(poll || {});
            console.log("Creating poll with _id " + poll._id);
          })
          .catch((err) => {
            res.status(400).json({
              code: 400,
              message: `ERROR post:polls -> ${err}`,
            });
            console.error(`ERROR post:polls -> ${err}`);
          });
      } catch (err) {
        res.status(400).json({
          code: 400,
          message: "Bad request post:polls",
        });
        console.error(`ERROR post:polls -> ${err}`);
      }
    });
  }

  readAll() {
    this.app.get("/polls/", (req, res) => {
      try {
        this.PollModel.find()
          .then((poll) => {
            res.status(200).json(poll);
            console.log("Returning all polls");
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR read:polls/ -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR read:polls/ -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  readId() {
    this.app.get("/polls/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the read:polls/:id query",
          });
          return;
        }

        this.PollModel.findById(req.params.id)
          .then((poll) => {
            res.status(200).json(poll || {});
            if (poll) console.log("Returning poll with _id " + poll._id);
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR read:polls/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR read:polls/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  update() {
    this.app.put("/polls/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the put:polls/:id query",
          });
          return;
        }

        const options = { new: true, runValidators: true };

        this.PollModel.findByIdAndUpdate(req.params.id, req.body, options)
          .then((pollUpdated) => {
            res.status(200).json(pollUpdated || {});
            if (pollUpdated)
              console.log("Returning poll updated with _id " + pollUpdated._id);
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR put:polls/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR put:polls/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  delete() {
    this.app.delete("/polls/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the delete:polls/:id query",
          });
          return;
        }

        const options = { new: true, runValidators: true };

        this.PollModel.findByIdAndUpdate(
          req.params.id,
          { active: false },
          options
        )
          .then((poll) => {
            res.status(200).json(poll || {});
            if (poll) console.log("Deleting poll with _id " + poll._id);
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR delete:polls/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR delete:polls/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  run() {
    this.create();
    this.readAll();
    this.readId();
    this.update();
    this.delete();
  }
};
