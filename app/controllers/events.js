const EventModel = require("../models/event.js");
const GroupModel = require("../models/group.js");

module.exports = class Events {
  constructor(app, connect) {
    this.app = app;
    this.EventModel = connect.model("Event", EventModel);
    this.GroupModel = connect.model("Group", GroupModel);
    this.run();
  }

  create() {
    this.app.post("/events/", (req, res) => {
      try {
        const eventModel = new this.EventModel(req.body);

        eventModel
          .save()
          .then((event) => {
            // res.status(200).json(event || {});
            console.log("Creating event with _id " + event._id);

            const options = { new: true, runValidators: true };

            this.GroupModel.findById(event.groupId)
              .then((group) => {
                this.EventModel.findByIdAndUpdate(
                  event._id,
                  {
                    members: [].concat(
                      event.members,
                      group.members
                    ) /* Merge the two arrays */,
                  },
                  options
                )
                  .then((event) => {
                    res.status(200).json(event || {});
                    if (event)
                      console.log(
                        "Adding group members to event with _id " + event._id
                      );
                  })
                  .catch((err) => {
                    res.status(400).json({
                      status: 400,
                      message: `ERROR Adding all members of group with _id ${group._id} to event with _id ${event._id} -> ${err}`,
                    });
                  });
              })
              .catch((err) => {
                res.status(400).json({
                  status: 400,
                  message: `ERROR finding group of event with _id ${event._id} -> ${err}`,
                });
              });
          })
          .catch((err) => {
            res.status(400).json({
              code: 400,
              message: `ERROR post:events -> ${err}`,
            });
            console.error(`ERROR post:events -> ${err}`);
          });
      } catch (err) {
        res.status(400).json({
          code: 400,
          message: "Bad request post:events",
        });
        console.error(`ERROR post:events -> ${err}`);
      }
    });
  }

  readAll() {
    this.app.get("/events/", (req, res) => {
      try {
        this.EventModel.find()
          .then((event) => {
            let eventFiltered = JSON.parse(JSON.stringify(event));

            if (req.body.userId == undefined || req.body.userId.length != 24) {
              eventFiltered = eventFiltered.filter((e) => e.type == "public");
            } else {
              eventFiltered = eventFiltered.filter(
                (e) =>
                  e.type == "public" ||
                  e.members.includes(req.body.userId) ||
                  e.organizers.includes(req.body.userId)
              );
            }

            res.status(200).json(eventFiltered || {});
            console.log(
              "Returning all public events or events where the userId given is present"
            );
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR read:events/ -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR read:events/ -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  readId() {
    this.app.get("/events/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the read:events/:id query",
          });
          return;
        }

        this.EventModel.findById(req.params.id)
          .then((event) => {
            let eventFiltered = JSON.parse(JSON.stringify(event));

            if (eventFiltered && eventFiltered.type != "public") {
              eventFiltered =
                eventFiltered.members.includes(req.body.userId) ||
                eventFiltered.organizers.includes(req.body.userId)
                  ? eventFiltered
                  : {};
            }

            res.status(200).json(eventFiltered || {});
            if (eventFiltered)
              console.log(
                "Returning event with _id " +
                  event._id +
                  " (empty if the event is not public and the userId given is not present)"
              );
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR read:events/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR read:events/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  update() {
    this.app.put("/events/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the put:events/:id query",
          });
          return;
        }

        const options = { new: true, runValidators: true };

        this.EventModel.findByIdAndUpdate(req.params.id, req.body, options)
          .then((eventUpdated) => {
            res.status(200).json(eventUpdated || {});
            if (eventUpdated)
              console.log(
                "Returning event updated with _id " + eventUpdated._id
              );
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR put:events/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR put:events/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  delete() {
    this.app.delete("/events/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the delete:events/:id query",
          });
          return;
        }

        const options = { new: true, runValidators: true };

        this.EventModel.findByIdAndUpdate(
          req.params.id,
          { active: false },
          options
        )
          .then((event) => {
            res.status(200).json(event || {});
            if (event) console.log("Deleting event with _id " + event._id);
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR delete:events/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR delete:events/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  run() {
    this.create();
    this.readAll();
    this.readId();
    this.update();
    this.delete();
  }
};
