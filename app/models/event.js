const { Schema } = require("mongoose");

const EventSchema = new Schema(
  {
    active: {
      type: Boolean,
      default: true,
    },
    groupId: {
      type: Schema.Types.ObjectId,
      ref: "Group",
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    description: String,
    dateStart: {
      type: Date,
      required: true,
    },
    dateEnd: {
      type: Date,
      required: true,
    },
    place: String,
    photo: String,
    type: {
      type: String,
      required: true,
      match: /(public|private)/gi,
    },
    members: {
      type: [Schema.Types.ObjectId],
      ref: "User",
      validate: (array) => array == null || array.length > 0,
    },
    organizers: {
      type: [Schema.Types.ObjectId],
      ref: "User",
      validate: (array) => array == null || array.length > 0,
    },
  },
  {
    collection: "events",
    minimize: false,
    versionKey: false,
  }
).set("toJSON", {
  transform: (doc, ret) => {
    ret.id = ret._id;
    delete ret._id;
  },
});

module.exports = EventSchema;
