const { Schema } = require("mongoose");

const messageSubSchema = new Schema({
  text: {
    type: String,
    required: true,
  },
  postUser: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  postDate: {
    type: Date,
    required: true,
  },

  comments: [
    {
      text: String,
      postUser: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true,
      },
      postDate: {
        type: Date,
        required: true,
      },
    },
  ],
});

const ThreadSchema = new Schema(
  {
    active: {
      type: Boolean,
      default: true,
    },
    name: {
      type: String,
      required: true,
    },
    sourceType: {
      type: String,
      required: true,
      match: /(group|event)/gi,
    },
    sourceId: {
      type: Schema.Types.ObjectId,
      ref: "Group",
      required: true,
    },

    messages: [messageSubSchema],
  },
  {
    collection: "threads",
    minimize: false,
    versionKey: false,
  }
).set("toJSON", {
  transform: (doc, ret) => {
    ret.id = ret._id;
    delete ret._id;
  },
});

module.exports = ThreadSchema;
