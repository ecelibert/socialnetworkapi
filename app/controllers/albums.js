const AlbumModel = require("../models/album.js");

module.exports = class Albums {
  constructor(app, connect) {
    this.app = app;
    this.AlbumModel = connect.model("Album", AlbumModel);
    this.run();
  }

  create() {
    this.app.post("/albums/", (req, res) => {
      try {
        const albumModel = new this.AlbumModel(req.body);

        albumModel
          .save()
          .then((album) => {
            res.status(200).json(album || {});
            console.log("Creating album with _id " + album._id);
          })
          .catch((err) => {
            res.status(400).json({
              code: 400,
              message: `ERROR post:albums -> ${err}`,
            });
            console.error(`ERROR post:albums -> ${err}`);
          });
      } catch (err) {
        res.status(400).json({
          code: 400,
          message: "Bad request post:albums",
        });
        console.error(`ERROR post:albums -> ${err}`);
      }
    });
  }

  readAll() {
    this.app.get("/albums/", (req, res) => {
      try {
        this.AlbumModel.find()
          .then((album) => {
            res.status(200).json(album);
            console.log("Returning all albums");
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR read:albums/ -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR read:albums/ -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  readId() {
    this.app.get("/albums/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the read:albums/:id query",
          });
          return;
        }

        this.AlbumModel.findById(req.params.id)
          .then((album) => {
            res.status(200).json(album || {});
            if (album) console.log("Returning album with _id " + album._id);
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR read:albums/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR read:albums/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  update() {
    this.app.put("/albums/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the put:albums/:id query",
          });
          return;
        }

        const options = { new: true, runValidators: true };

        this.AlbumModel.findByIdAndUpdate(req.params.id, req.body, options)
          .then((albumUpdated) => {
            res.status(200).json(albumUpdated || {});
            if (albumUpdated)
              console.log(
                "Returning album updated with _id " + albumUpdated._id
              );
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR put:albums/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR put:albums/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  delete() {
    this.app.delete("/albums/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the delete:albums/:id query",
          });
          return;
        }

        const options = { new: true, runValidators: true };

        this.AlbumModel.findByIdAndUpdate(
          req.params.id,
          { active: false },
          options
        )
          .then((album) => {
            res.status(200).json(album || {});
            if (album) console.log("Deleting album with _id " + album._id);
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR delete:albums/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR delete:albums/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  run() {
    this.create();
    this.readAll();
    this.readId();
    this.update();
    this.delete();
  }
};
