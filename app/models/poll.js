const { Schema } = require("mongoose");

const questionSubSchema = new Schema({
  text: {
    type: String,
    required: true,
  },

  answers: [
    {
      text: {
        type: String,
        required: true,
      },
      number: {
        type: Number,
        required: true,
      },
    },
  ],

  userAnswers: [
    {
      postUser: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true,
      },
      postDate: {
        type: Date,
        required: true,
      },
      answerNumber: {
        type: Number,
        required: true,
      },
    },
  ],
});

const PollSchema = new Schema(
  {
    active: {
      type: Boolean,
      default: true,
    },
    name: {
      type: String,
      required: true,
    },
    eventId: {
      type: Schema.Types.ObjectId,
      ref: "Event",
      required: true,
    },
    questions: [questionSubSchema],
  },
  {
    collection: "polls",
    minimize: false,
    versionKey: false,
  }
).set("toJSON", {
  transform: (doc, ret) => {
    ret.id = ret._id;
    delete ret._id;
  },
});

module.exports = PollSchema;
