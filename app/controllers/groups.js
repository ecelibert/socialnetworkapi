const GroupModel = require("../models/group.js");

module.exports = class Groups {
  constructor(app, connect) {
    this.app = app;
    this.GroupModel = connect.model("Group", GroupModel);
    this.run();
  }

  create() {
    this.app.post("/groups/", (req, res) => {
      try {
        const groupModel = new this.GroupModel(req.body);

        groupModel
          .save()
          .then((group) => {
            res.status(200).json(group || {});
            console.log("Creating group with _id " + group._id);
          })
          .catch((err) => {
            res.status(400).json({
              code: 400,
              message: `ERROR post:groups -> ${err}`,
            });
            console.error(`ERROR post:groups -> ${err}`);
          });
      } catch (err) {
        res.status(400).json({
          code: 400,
          message: "Bad request post:groups",
        });
        console.error(`ERROR post:groups -> ${err}`);
      }
    });
  }

  readAll() {
    this.app.get("/groups/", (req, res) => {
      try {
        this.GroupModel.find()
          .then((group) => {
            let groupFiltered = JSON.parse(JSON.stringify(group));

            if (req.body.userId == undefined || req.body.userId.length != 24) {
              groupFiltered = groupFiltered.filter((e) => e.type == "public");
            } else {
              groupFiltered = groupFiltered.filter(
                (e) =>
                  e.type == "public" ||
                  e.members.includes(req.body.userId) ||
                  e.admins.includes(req.body.userId)
              );
            }

            res.status(200).json(groupFiltered || {});
            console.log(
              "Returning all public groups or groups where the userId given is present"
            );
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR read:groups/ -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR read:groups/ -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  readId() {
    this.app.get("/groups/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the read:groups/:id query",
          });
          return;
        }

        this.GroupModel.findById(req.params.id)
          .then((group) => {
            let groupFiltered = JSON.parse(JSON.stringify(group));

            if (groupFiltered && groupFiltered.type != "public") {
              groupFiltered =
                groupFiltered.members.includes(req.body.userId) ||
                groupFiltered.admins.includes(req.body.userId)
                  ? groupFiltered
                  : {};
            }

            res.status(200).json(groupFiltered || {});
            if (groupFiltered)
              console.log(
                "Returning group with _id " +
                  group._id +
                  " (empty if the group is not public and the userId given is not present)"
              );
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR read:groups/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR read:groups/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  update() {
    this.app.put("/groups/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the put:groups/:id query",
          });
          return;
        }

        const options = { new: true, runValidators: true };

        this.GroupModel.findByIdAndUpdate(req.params.id, req.body, options)
          .then((groupUpdated) => {
            res.status(200).json(groupUpdated || {});
            if (groupUpdated)
              console.log(
                "Returning group updated with _id " + groupUpdated._id
              );
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR put:groups/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR put:groups/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  delete() {
    this.app.delete("/groups/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the delete:groups/:id query",
          });
          return;
        }

        const options = { new: true, runValidators: true };

        this.GroupModel.findByIdAndUpdate(
          req.params.id,
          { active: false },
          options
        )
          .then((group) => {
            res.status(200).json(group || {});
            if (group) console.log("Deleting group with _id " + group._id);
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR delete:groups/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR delete:groups/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  run() {
    this.create();
    this.readAll();
    this.readId();
    this.update();
    this.delete();
  }
};
