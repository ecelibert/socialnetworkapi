const { Schema } = require("mongoose");

const UserSchema = new Schema(
  {
    active: {
      type: Boolean,
      default: true,
    },
    username: {
      type: String,
      required: [true, "The user name is required"],
    },
    email: {
      type: String,
      required: [true, "The user email is required"],
      unique: [true, "The user email provided already exists"],
      match: [/.+@.+\..+/],
      lowercase: true,
      trim: true,
      index: true,
    },
    password: {
      type: String,
      required: [true, "The user password is required"],
    },
  },
  {
    collection: "users",
    minimize: false,
    versionKey: false,
  }
).set("toJSON", {
  transform: (doc, ret) => {
    ret.id = ret._id;
    delete ret._id;
  },
});

module.exports = UserSchema;
