const UserModel = require("../models/user.js");

module.exports = class Users {
  constructor(app, connect) {
    this.app = app;
    this.UserModel = connect.model("User", UserModel);
    this.run();
  }

  create() {
    this.app.post("/users/", (req, res) => {
      try {
        const userModel = new this.UserModel(req.body);

        userModel
          .save()
          .then((user) => {
            res.status(200).json(user || {});
            console.log("Creating user with _id " + user._id);
          })
          .catch((err) => {
            res.status(400).json({
              code: 400,
              message: `ERROR create:users -> ${err}`,
            });
            console.error(`ERROR create:users -> ${err}`);
          });
      } catch (err) {
        res.status(400).json({
          code: 400,
          message: "Bad request create:users",
        });
        console.error(`ERROR create:users -> ${err}`);
      }
    });
  }

  readAll() {
    this.app.get("/users/", (req, res) => {
      try {
        this.UserModel.find()
          .then((user) => {
            res.status(200).json(user);
            console.log("Returning all users");
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR read:users/ -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR read:users/ -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  readId() {
    this.app.get("/users/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the read:users/:id query",
          });
          return;
        }

        this.UserModel.findById(req.params.id)
          .then((user) => {
            res.status(200).json(user || {});
            if (user) console.log("Returning user with _id " + user._id);
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR read:users/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR read:users/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  update() {
    this.app.put("/users/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the put:users/:id query",
          });
          return;
        }

        const options = { new: true, runValidators: true };

        this.UserModel.findByIdAndUpdate(req.params.id, req.body, options)
          .then((userUpdated) => {
            res.status(200).json(userUpdated || {});
            if (userUpdated)
              console.log("Returning user updated with _id " + userUpdated._id);
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR put:users/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR put:users/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  delete() {
    this.app.delete("/users/:id", (req, res) => {
      try {
        if (!req.params.id || req.params.id.length != 24) {
          res.status(400).json({
            status: 400,
            message:
              "Bad Request : A correct ObjectId parameter is needed in the delete:users/:id query",
          });
          return;
        }

        const options = { new: true, runValidators: true };

        this.UserModel.findByIdAndUpdate(
          req.params.id,
          { active: false },
          options
        )
          .then((user) => {
            res.status(200).json(user || {});
            if (user) console.log("Deleting user with _id " + user._id);
          })
          .catch((err) => {
            res.status(400).json({
              status: 400,
              message: `ERROR delete:users/:id -> ${err}`,
            });
          });
      } catch (err) {
        console.error(`ERROR delete:users/:id -> ${err}`);

        res.status(500).json({
          status: 500,
          message: "Internal Server Error",
        });
      }
    });
  }

  run() {
    this.create();
    this.readAll();
    this.readId();
    this.update();
    this.delete();
  }
};
