module.exports = {
  development: {
    express: {
      port: 3000,
    },
    mongodb: {
      host: "mongodb://localhost:27017/socialnetwork",
    },
  },
  production: {
    express: {
      port: 3001,
    },
    mongodb: {
      host: "mongodb://localhost:27017/socialnetwork",
    },
  },
};
