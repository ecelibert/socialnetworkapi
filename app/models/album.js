const { Schema } = require("mongoose");

const photoSubSchema = new Schema({
  link: {
    type: String,
    required: true,
  },
  postUser: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  postDate: {
    type: Date,
    required: true,
  },

  comments: [
    {
      text: String,
      postUser: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true,
      },
      postDate: {
        type: Date,
        required: true,
      },
    },
  ],
});

const AlbumSchema = new Schema(
  {
    active: {
      type: Boolean,
      default: true,
    },
    name: {
      type: String,
      required: true,
    },
    eventId: {
      type: Schema.Types.ObjectId,
      ref: "Event",
      required: true,
    },
    photos: [photoSubSchema],
  },
  {
    collection: "albums",
    minimize: false,
    versionKey: false,
  }
).set("toJSON", {
  transform: (doc, ret) => {
    ret.id = ret._id;
    delete ret._id;
  },
});

module.exports = AlbumSchema;
